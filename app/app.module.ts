import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { AppRoutingModule } from "./app.routing";
import { AppComponent } from "./app.component";

import { ItemService } from "./item/item.service";
import { ItemsComponent } from "./item/items.component";
import { ItemDetailComponent } from "./item/item-detail.component";

import { TNSFontIconModule } from 'nativescript-ngx-fonticon';

import { SIDEDRAWER_DIRECTIVES } from "nativescript-telerik-ui/sidedrawer/angular";
import {NavigationComponent} from "./components/navigation/navigation.component";
import {HomeComponent} from "./components/home/home.component";

@NgModule({
    bootstrap: [
        AppComponent
    ],
    imports: [
        NativeScriptModule,
        AppRoutingModule,
        TNSFontIconModule.forRoot({
            'fa': 'font-awesome.css'
        })
    ],
    declarations: [
		NavigationComponent,
		AppComponent,
		ItemsComponent,
		ItemDetailComponent,
		HomeComponent,
        SIDEDRAWER_DIRECTIVES
    ],
    providers: [
        ItemService
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class AppModule { }
