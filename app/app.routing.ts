import {NgModule} from "@angular/core";
import {NativeScriptRouterModule} from "nativescript-angular/router";
import {Routes} from "@angular/router";
import {ItemDetailComponent} from "./item/item-detail.component";
import {NavigationComponent} from "./components/navigation/navigation.component";
import {HomeComponent} from "./components/home/home.component";
import {ItemsComponent} from "./item/items.component";

const routes: Routes = [
	{
		path: '',
		redirectTo: '/navigation',
		pathMatch: 'full'
	},
	{
		path: "navigation",
		component: NavigationComponent,
		children: [
			// '/home' loaded into `router-outlet` in main content
			{path: "", component: HomeComponent},

			{path: "home", component: HomeComponent},

			// '/home/otherPath' loaded into `router-outlet` in main content
			{path: "otherPath", component: ItemDetailComponent}
		]
	},
	{path: "item/:id", component: ItemDetailComponent},
	{path: "home", component: HomeComponent}

	// '/someNavPage' pushed on nav stack using `page-router-outlet` (ie, push on a detail view with no drawer)
	// { path: "someNavPage", component: NavPageComponent },
	// etc.
];

@NgModule({
	imports: [NativeScriptRouterModule.forRoot(routes)],
	exports: [NativeScriptRouterModule]
})
export class AppRoutingModule {
}