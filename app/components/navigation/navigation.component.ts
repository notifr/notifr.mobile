/**
 * Created by inuhiu@ir-tech.ch on 08.03.17.
 */
// angular
import {Component, ViewChild, ChangeDetectorRef, Inject, OnInit, AfterViewInit} from '@angular/core';
import {Router, NavigationEnd} from '@angular/router';

// nativescript
import {RadSideDrawerComponent, SideDrawerType} from 'nativescript-telerik-ui/sidedrawer/angular';
import {DrawerTransitionBase, SlideInOnTopTransition} from 'nativescript-telerik-ui/sidedrawer';
import {Page} from "ui/page";

@Component({
	moduleId: module.id,
	selector: 'navigation',
	templateUrl: 'navigation.component.html'
})
export class NavigationComponent implements OnInit, AfterViewInit {
	@ViewChild(RadSideDrawerComponent)
	public drawerComponent: RadSideDrawerComponent;

	private mySideDrawerTransition: DrawerTransitionBase;
	private drawer: SideDrawerType;

	/**
	 * @param page
	 * @param changeDetectionRef
	 * @param router
	 */
	constructor(@Inject(Page) private page: Page,
				private changeDetectionRef: ChangeDetectorRef,
				private router: Router) {
		this.page.on("loaded", this.onLoaded, this);
	}

	/**
	 * @returns {DrawerTransitionBase}
	 */
	public get sideDrawerTransition(): DrawerTransitionBase {
		return this.mySideDrawerTransition;
	}

	/**
	 * Toggles the navigation
	 */
	public toggle(): void {
		this.drawer.toggleDrawerState();
	}

	/**
	 * Triggers event when page was loaded
	 * @param args
	 */
	public onLoaded(): void {
		this.mySideDrawerTransition = new SlideInOnTopTransition();
	}

	/**
	 * Angular lifecycle
	 */
	public ngOnInit(): void {
		this.router.events.subscribe((e) => {
			if (e instanceof NavigationEnd) {
				this.drawer.closeDrawer();
			}
		});
	}

	/**
	 * Angular lifecycle
	 */
	public ngAfterViewInit(): void {
		this.drawer = this.drawerComponent.sideDrawer;
		this.changeDetectionRef.detectChanges();
	}
}